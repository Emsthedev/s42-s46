const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Product = require('../models/Product');


//registerUser

module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName : reqBody.firstName,
        lastName: reqBody.lastName,
        userName: reqBody.userName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10)
    })
    return newUser.save().then((user, error) =>{
        if(error){
            return false
        } else {
            return true
        }
    })
}

//loginUser
module.exports.loginRegUser = (reqBody) => {
    return User.findOne({
        email: reqBody.email
    })
    .then(result => {
        if (result == null){
            return false
        } else{
            //compareSync(<data to compare>, <encrypted password>)
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
            if(isPasswordCorrect){
                return {access: auth.createAccessToken(result)}
            } else{
                return false
            }
        }
    })
};

//s45 Create order for Non Admin
module.exports.order = async (data) => {

    if(data.isAdmin){
		return await "you are an Admin, Only users can make an Order"
	} else {
       
    let isUserUpdated = await User.findById(data.userId) 
    .then(user => {
        user.orders.push ({
            productId: data.productId,
            productName: data.productName,
            quantity: data.quantity
        })

        return user.save().then((user,error) =>{
            if (error){
                return false
            } else { 
                return true
            }
        })
    })

    let isProductUpdated = await Product.findById(data.productId)
    .then(product => {
                product.orderedUser.push({
                    userId : data.userId

                })

                return product.save().then((product,error) =>{
                    if (error){
                        return false
                    } else { 
                        return true
                    }
                })


    })

    if (isUserUpdated && isProductUpdated){
        return "User successfully ordered" //true
    } else {
        return "Client cannot order. Error encountered" //false
    }
} //main else

}; // end module

//s45 Retrieve User Details

module.exports.getUserDetails = (userData) => {
    return User.findById(userData.id)
    .then(result => {
        console.log(userData)
        if(result == null) {
          return false
        }else {
        console.log(result)
        result.password="******";
        return result
        }
    })
};