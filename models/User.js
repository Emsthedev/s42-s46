const mongoose = require ('mongoose');

//Creating of Schema

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "First Name is Required"]
    },
    lastName: {
        type: String,
        required: [true, "Last Name is Required"]
    },
    userName: {
        type: String,
        required: [true, "Username is Required"]
    },
    email: {
        type: String,
        required: [true, "Email is Required"]
    },
    password: {
        type: String,
        required: [true, "Password is Required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    orders:[{
        productId: {
            type: String,
            required: [true, "Product Id is required"]
        },
        productName: {
            type: String,
            required: [true, "Product Name is required"]
        },
        quantity: {
            type:Number
        },
        totalAmount: {
            type: Number
        },
        purchasedOn: {
                type: Date,
                default: new Date(),
                immutable: true
            }
        }]
});
module.exports = mongoose.model("User", userSchema)
