const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth')


//Creating Routes



//register route - create user in db
router.post ("/register", (req,res) => {
    userController.registerUser(req.body)
    .then(resultFromController => 
        res.send(resultFromController))
});

//auth
router.post("/login", (req, res)=>{
    userController.loginRegUser(req.body)
    .then(resultFromController => 
        res.send(resultFromController))
});
//s45 Create Order for user (Non-Admin)

router.post('/order', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	// if(userData.isAdmin){
	// 	res.send("you are an Admin, Only users can make an Order")
	// } else {
	// 	let data = {
	// 		userId: userData.id,
	// 		productId: req.body.productId
	// 	}
    let data = {
        userId: userData.id,
        isAdmin: userData.isAdmin,
        productId: req.body.productId,
        productName: req.body.productName,
        quantity: req.body.quantity
    }
		userController.order(data)
        .then(resultFromController => 
            res.send(resultFromController));
	//}
});


//s45 Retrieve User Details
router.get("/details", auth.verify, (req, res)=>{
    
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)

    userController.getUserDetails({id: userData.id})
    .then(resultFromController => 
        res.send(resultFromController))
});





module.exports = router;
